Dobrý den, ahoj, tohle je popis jednoduché rpg hry pro semestrální práci na předmět PPC.
Složka obsahuje malou ukázku.

#kompilace
-složka obsahuje tři knihovny (.hpp), jeden soubor (.cpp) a tři soubory s texty (.txt). Celý program je psán v linuxu-Ubuntu a spouští se v jeho příkazovém řádku (např. WSL).
-kompiluje se pomocí kompilátoru clang++

Návod na kompilaci
Otevřete linuxový terminál a zkompilujte soubor main.cpp. Za to přidejte -o "a název který má mít zkompilovaný soubor. Např. "clang++ ./main.cpp -o main"
To je z kompilace vše.

##spuštěnní
Hra se již spouští jednoduchým příkazem "./main" nebo název který jste zadali v kompilaci.

###hra

1) tutoriál - po spuštění rpojdtete lehkým tutoriálem, kde je lehce popsán průběh hry a vyzkoušíte si mechanismus souboje. Pak už následuje první úkol.
2) hlavní menu
-úkol - spustí úkol, kde budete procházet pár rozhodnutími a končí soubojem
-obchod - v obchodu můžete jak prodávat, tak kupovat předměty. Abyste si mohli předmět koupit, musíte mít dostatek peněz. Pokud máte, předmět se vám přidá do inventáře. 
            To neplatí pro předmět lektvar, který vám okamžitě přidává 50 HP a do inventáře se nedává. Může se stát, že si koupíte jednu věc několikrát. Pak při prodeji, když máte zadat ID předmětu, 
            to ID může mít více věcí. Prodá se však jen jedna.
-inventář - zobrazí jaké věci máte v inventáři, včetně peněz
-staty - zobrazuje vaše statistiky, jako level, životy, útok, XP apod.
-Konec - ukončuje hru (bez uložení)

3) souboje

-Pokud se dostanete do souboje, na začátku se vypíše tabulka s útokem a životy obou účastníku. Pak začíná souboj.
-Ten probíhá tak, že určujete jestli chcete zaútočit nebo se pokusit o útěk.
    a)útok - vypíše se kolik jste protivníkovi ubrali životů a kolik jich zbývá, pak útočí protivník. Útok se počítá tak, že se sečtou útoky VŠECH zbraní a předmětů v inventáři. To však neznamená, že přesně tolik uberete.
            Rozsah útoku je nájodně +-50 váš útok, podle toho jak trefíte.
    b)útěk - pokud se rozhodnete pro útěk, máte 50% šanc, že se povede. Pokud ano, ukončíte sice úkol, ale nedostanete žádné peníze ani XP. Pokud se nepovede, protivník vám zasadí větší ránu.