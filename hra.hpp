#include <iostream>
#include <vector>
#include <fstream>
#include <algorithm>
#include <iterator>
//#include "inventar.hpp"
#include "hrac.hpp"
#include <unistd.h>
#include <dirent.h>

struct List
{
    std::string text;
    int volby;
    std::vector<int> roz;
    ~List()
    {
        roz.clear();
    }
};

struct Hra
{
    ~Hra()
    {
        kniha.clear();
        quest.clear();
    }
    char hlavni_volba;
    char volba;
    char c;
    std::vector<List> kniha;
    Inventar inv; //inventar, ve kterem jsou vsechny predmety
    player hrac;
    creature protivnik; //protivnik
    std::vector<std::string> quest; //vektor do ktereho se nacitaji nazvy souboru

    //funkce
    void nahraj(std::string filename);
    void hraj(int next = 0);
    void tutorial();
    void soubory();
    void obchudek();
    void obchod();
    int zkontrolujVstup(int vol, int min, int max, int vyjimka = 0);
};

void Hra::nahraj(std::string filename) //nahrava questy
{
    std::ifstream is(filename);
    while (!is.eof())
    {
        List tmp;
        std::getline(is, tmp.text);
        is >> tmp.volby;
        if (tmp.volby > 0)
            std::copy_n(std::istream_iterator<int>(is), tmp.volby, std::back_inserter(tmp.roz));
        do
        {
            is.get(c);
        } while (c != '\n');
        this->kniha.push_back(tmp);
    }
}
void Hra::hraj(int next) //hraje questy, řídí se podle voleb
{
    do
    {
        List tmp = this->kniha[next];
        std::cout << tmp.text << std::endl;
        if (tmp.volby > 0)
        {
            std::cout << "> ";
        zpet:
            std::cin >> volba;
            if (zkontrolujVstup(volba, 1, tmp.volby) == 1)
            {
                goto zpet;
            }
            int volba_2 = volba - 48;

            next = tmp.roz[volba_2 - 1];
        }
        else
            next = 0;
    } while (next > 0);
    kniha.clear();
}
void Hra::tutorial() //uvod do hry, vyzkouseni boje a prvni ukol
{
    std::cout << "Vitej ve hre hrdino. Jake je tve ctene jmeno?" << std::endl;
    std::cin >> hrac.name;
    hrac.vytvor(1, hrac.name, 0, 100, 100, 35, 500);
    protivnik.vytvor(protivnik.getName(), 30, 100, 350, 1, 100);
    sleep(1);
    std::cout << "Zde si muzes vyzkouset prubeh souboje. Mas dve moznosti, bud zautocit, nebo utect. Utok se pocita podle tveho inventare +- 50. Pokud se rozhodnes utect, mas 50% sanci. Pokuud to nevyjde, dostanes velky hit od prisery. Pojdme na to!" << std::endl;
    sleep(15);
    hrac.boj(protivnik);
    hrac.hp = 500;
    std::cout << "No tak to nebylo tak tezke ne? Ted uz jsi pripraven na svou prvni cestu, hodne stesti!" << std::endl;
    std::cout << std::string(86, '-') << std::endl;
    sleep(3);
    //prvni quest
    nahraj(quest[0]);
    hraj();
    hrac.inv.inv.push_back(inv.inv[0]); //hrac dostava prvni mec
    hrac.inv.list(hrac.coins_p);
}
void Hra::soubory() // nacteni souboru s ukoly do vektoru
{
    quest.push_back("mesto.txt");
    quest.push_back("q1_a.txt");
    quest.push_back("q1_b.txt");
}

void Hra::obchudek() //obchod
{
    char obe;
    char volbicka_1;
    sleep(1);
    std::cout << std::string(55, '-') << std::endl;
    std::cout << "Chces neco koupit (1) nebo prodat (2)?" << std::endl;
oba:
    std::cin >> obe;
    if (zkontrolujVstup(obe, 1, 2) == 1) 
    {
        goto oba;
    }

    int ob = obe - 48;
    if (ob == 1) //koupit
    {
        std::cout << "Obchod:" << std::setw(15) << "Penize: " << hrac.coins_p << std::endl
                  << std::endl;
        std::cout << std::setw(3) << "ID" << std::setw(25) << "Jmeno" << std::setw(10) << "Utok" << std::setw(6) << "Cena" << std::endl;

        for (int i = 1; i < inv.inv.size(); i++)
        {
            std::cout << std::setw(3) << inv.inv[i]->id << std::setw(25) << inv.inv[i]->name << std::setw(10) << inv.inv[i]->attack << std::setw(6) << inv.inv[i]->price << std::endl;
        }
        std::cout << std::setw(3) << inv.inv[inv.inv.size() - 1]->id + 1 << std::setw(25) << "Tentokrat nic." << std::endl;
    volba:
        std::cin >> volbicka_1;

        //volbicka -= 1;
        if (zkontrolujVstup(volbicka_1, 1, 7, 8) == 0)
        {
            int volbicka_2 = volbicka_1 - 48;
            if (inv.inv[volbicka_2]->price <= hrac.coins_p)
            {
                if (volbicka_2 == 7) //lektvar, nedava se do inventare, ale rovnou se oricitaji HP
                {
                    hrac.coins_p = hrac.coins_p - inv.inv[volbicka_2]->price;
                    hrac.hp += 50;
                    hrac.staty();
                }
                else //odecitani penez, pridavani predmetu do inventare, vypisovani
                {
                    hrac.inv.add(inv.inv[volbicka_2]);
                    hrac.coins_p = hrac.coins_p - inv.inv[volbicka_2]->price;
                    hrac.inv.list(hrac.coins_p);
                }
            }
            else
            {
                std::cout << "Bohuzel na tuto vec nemas dost penez. Zkus to znovu." << std::endl;
                goto volba;
            }
        }
        else
        {
            if ((volbicka_1 - 48) != 8)
                goto volba;
        }

        std::cout << std::string(44, '-') << std::endl;
    }
    else if (ob == 2) //prodat
    {
        std::cout << "Co bys chtel prodat?" << std::endl;
        hrac.inv.list(hrac.coins_p);
        std::cout << std::setw(3) << inv.inv[inv.inv.size() - 1]->id + 1 << std::setw(25) << "Tentokrat nic." << std::endl;

        //std::cout << hrac.inv.inv.size() << " zpet" << std::endl;
    volbaa:
        std::cin >> volbicka_1;
        //volbicka -=1;

        if (zkontrolujVstup(volbicka_1, 1, 8, 0) == 0)
        {
            int volbicka_2 = volbicka_1 - 48;
            int volba = 0;
            for (int i = 0; i < hrac.inv.inv.size(); i++)
            {
                if (hrac.inv.inv[i]->id == volbicka_2 && volba != 1) //pridavani penez, odebirani predmetu
                {
                    hrac.coins_p += hrac.inv.inv[i]->price;
                    hrac.inv.inv = hrac.vektor(hrac.inv.inv, volbicka_2);
                    volba = 1;
                }
            }
            sleep(1);
            hrac.inv.list(hrac.coins_p);
        }
        else
        {
            //std::cout << "Spatne jsi to zadal, zkus to znovu!" << std::endl;
            goto volbaa;
        }
    }
}

void Hra::obchod() //hlavicka obchodu
{
    std::cout << "\nVitej hrdino, prejes si u me neco zakoupit nebo prodat?" << std::endl;
    obchudek();
}

int Hra::zkontrolujVstup(int vol, int min, int max, int vyjimka) //kontroluje jestli vstup je cislo nebo pismeno, da se navolit v jakem rozsahu ma cislo byt
{
    if (isdigit(vol))
    {
        int volba = vol - 48;
        if (volba < min || volba > max)
        {
            if (vyjimka == 0)
                std::cout << "\nZadali jste spatnoou volbu :(. Zkuste to znovu" << std::endl;
            return 1;
        }
        return 0;
    }
    else
    {
        std::cout << "Nezadali jste cislo! Zkuste to znovu..." << std::endl;
        return 1;
    }
}
