#include <string>
#include "inventar.hpp"
#include <iomanip>
#include <unistd.h>

//vektor jmen protivniku, ze ktereho se pak nahodne vybira
std::vector<std::string> JmenaMobu = {"Strasidlo", "Hovnival", "Glum", "Zombie", "Pavouk", "Carodejka", "Zlodej", "Zly Santa", "Kyklop", "Trpaslik", "Skret", "Temny elf"};

class creature //trida protivnika
{
public:
    std::string name;
    int attack;
    int killXP; //XP ktere dostane hrac, kdyz ho porazi
    int hp;
    int level;
    int coins_c; //penize ktere dostane hrac, kdyz ho porazi
    bool konec;

    std::string getName() //nahodne vybira jmeno
    {
        name = JmenaMobu[rand() % JmenaMobu.size()];
        return name;
    }

    void vytvor(std::string n, int a, int k, int h, int lvl, int c) //neco jako konstruktor
    {
        name = n;
        attack = a;
        killXP = k;
        hp = h;
        level = lvl;
        coins_c = c;
    }

    creature(){};
    ~creature(){};
};

class player //trida hrace
{
public:
    int level;
    std::string name;
    int exp;
    int exp_next; //XP pro dalsi level
    int med_attack; //v podstate utok
    int coins_p;
    std::string AnB; //volba hrace v boji
    int hp;
    Inventar inv; //inventar hrace

    player(){};
    ~player(){};

    //funkce
    void boj(creature zlo);
    void zmenaxp(int zloe);
    void death();
    int AneboB();
    void vytvor(int lvl, std::string n, int exp, int exp_n, int med, int c, int h);
    void spocitejatak();
    void staty();
    int getAttack();
    std::vector<Item *> vektor(std::vector<Item *> inv, int volbicka);
};

std::vector<Item *> player::vektor(std::vector<Item *> inv, int volbicka) //odstranuje predmet z inventare
{
    std::vector<Item *> inv2;
    int volba = 0;
    for (int i = 0; i < inv.size(); i++)
    {
        if (inv[i]->id == volbicka && volba != 1)
        {
            volba = 1;
        }
        else
        {
            inv2.push_back(inv[i]);
        }
    }
    return inv2;
}

int player::getAttack() //spocita utok, pomoci inventare
{
    srand(time(0));
    spocitejatak();
    int attack = 0;
    int mma = 0;
    mma = med_attack + 50;
    int mm = 0;
    mm = med_attack - 50;

    attack = mm + (rand() % (mma - mm + 1)); //hodnota utoku bude od utoku odchylena max o 50
    return attack;
}

void player::staty() //vypis statistik hrace
{
    spocitejatak();
    std::cout << std::string(21, '-') << std::endl;
    std::cout << "Tvoje staty:" << std::endl;
    std::cout
        << std::setw(17) << "Level: " << std::setw(4) << level << std::endl
        << std::setw(17) << "Prumerny utok: " << std::setw(4) << med_attack << std::endl
        << std::setw(17) << "HP: " << std::setw(4) << hp << std::endl
        << std::setw(17) << "XP: " << std::setw(4) << exp << std::endl
        << std::setw(17) << "XP pro lvlup: " << std::setw(4) << exp_next << std::endl;
    std::cout << std::string(21, '-') << std::endl;
    sleep(5);
}

void player::spocitejatak() //probere inventar hrace a secte vsechny utoky
{
    med_attack = 25 + level * 75;
    for (int i = 0; i < inv.inv.size(); i++)
    {
        med_attack += inv.inv[i]->attack;
    }
}

void player::vytvor(int lvl, std::string n, int ex, int exp_n, int med, int c, int h) //v podstate konstruktor
{
    level = lvl;
    name = n;
    exp = ex;
    exp_next = exp_n;
    med_attack = med;
    coins_p = c;
    hp = h;
}
void player::death() //smrt
{
    hp = 100;
    std::cout << "Bohuzel jsi zemrel v kaluzi krve a svych vnitrosti. Jak smutne. Z tohoto ukolu nic nebude." << std::endl;
    std::cout << "Dostavas dva elixir zdarma, HP: " << hp << std::endl;
}

void player::zmenaxp(int zloe) //pta se, jestli hrac nedosahl noveho leevelu, pokud ano, zvysi level a statistiky
{
    exp = exp + zloe;
    if (exp >= exp_next)
    {
        level += 1;
        int prebytek = 0;
        prebytek = exp - exp_next;
        exp = prebytek;
        std::cout << "Gratuluji, jsi zase o level silnejsi! Ted jsi lvl " << level << std::endl;
        sleep(2);

        exp_next = level * 100;
        med_attack += level * 75;
        hp += (level * 50);
    }
}

int player::AneboB() //pta se hrace, jakou volbu v boji zvolit
{
    std::cout << "Zmackni 1: utok, 2: utek" << std::endl;
    std::cin >> AnB;
    int volba = 0;
    try
    {
        volba = std::stoi(AnB);
        if (volba < 1 || volba > 2)
        {
            std::cout << "Zadali jste spatnoou volbu :(. Zkuste to znovu" << std::endl;
            AneboB();
        }
    }
    catch (const std::exception &e)
    {
        std::cout << "Nezadali jste cislo!" << std::endl;
    }
    return volba;
}

void player::boj(creature zlo) //bojovy mechanismus
{
    spocitejatak();
    std::cout << std::string(35, '-') << std::endl;
    std::cout << "Boj:" << std::endl;
    std::cout << std::setw(15) << "Jmeno" << std::setw(8) << "Utok" << std::setw(9) << "HP" << std::endl
              << std::setw(15) << name << std::setw(8) << med_attack << std::setw(9) << hp << std::endl
              << std::setw(15) << zlo.getName() << std::setw(8) << zlo.attack << std::setw(9) << zlo.hp << std::endl;
    sleep(2);
    std::cout << "\nZacinas!" << std::endl;
    zlo.konec = 1;
    while (zlo.hp > 0 && hp > 0)
    {
        int attacek = 0;
        if (hp <= 0)
        {
            hp = 0;
            death();
            break;
        }
        if (zlo.hp < 0)
        {
            zlo.hp = 0;
        }
        switch (AneboB())
        {
        case 1: //utok
            attacek = getAttack();
            zlo.hp = zlo.hp - attacek;
            hp = hp - zlo.attack;
            if (zlo.hp < 0)
            {
                zlo.hp = 0;
            }
            std::cout << std::string(35, '-') << std::endl;
            std::cout << std::setw(12) << "Dal jsi: " << std::setw(3) << attacek << " | " << std::setw(12) << "HP prisery: " << zlo.hp << std::endl;
            if (zlo.hp > 0)
            {
                std::cout << std::setw(12) << "Dostal jsi: " << std::setw(3) << zlo.attack << " | " << std::setw(12) << "Tvoje HP: " << hp << std::endl;
            }
            std::cout << std::string(35, '-') << std::endl;
            sleep(1);
            break;

            

        case 2: //utek, 50% sance
            if (rand() % 2 == 1)
            {
                if (level != 1)
                {
                    std::cout << "Utek byl uspesny. Uff.. Sice zijes ale z tohodle ukolu nic nebude." << std::endl;
                    std::cout << std::string(66, '-') << std::endl;
                }

                zlo.hp = -100;
                zlo.konec = 0;
                sleep(1);
            }
            else //pokud se utok nepovede, hrac dostane od protivnika vetsi utok nez obvykle
            {
                std::cout << std::string(45, '-') << std::endl;
                std::cout << "Utek se nepovedl :(. Dostavas hit bez obrany." << std::endl;
                hp = hp - (zlo.attack + zlo.level * 35);
                std::cout << std::setw(12) << "Dostal jsi: " << std::setw(3) << (zlo.attack + zlo.level * 35) << std::setw(12) << std::right << "Tvoje HP:" << hp << std::endl;
                std::cout << std::string(45, '-') << std::endl;
                sleep(1);
            }
            break;
        }
    }
    sleep(1);

    if (zlo.konec == 1)
    {
        std::cout << "Sundal jsi ho, gratuluju. Ziskavas " << zlo.killXP << " XP a " << zlo.coins_c << " zlataku" << std::endl;
        zmenaxp(zlo.killXP);
        coins_p += zlo.coins_c;
        sleep(2);
    }
    else if (zlo.konec == 0)
    {
        if (level == 1) //v prvnim boji je jiny text, apod.
        {
            std::cout << "Jakz takz jsi splnil ukol. I tak ziskavas " << zlo.killXP << " XP a " << zlo.coins_c << " zlataku" << std::endl;
            zmenaxp(zlo.killXP);
            coins_p += zlo.coins_c;
        }
    }

    std::cout << std::string(86, '-') << std::endl;
}
