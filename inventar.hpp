#include <vector>
#include <iomanip>
#include <unistd.h>

#ifndef __MAIN_HPP__
#define __MAIN_HPP__

class Item //trida predmetu
{
    public:
    int attack;
    int price;
    std::string name;
    int id;

    Item(std::string n, int a, int p, int i){ //konstruktor
        name = n;
        attack = a;
        price = p;
        id = i;
    };
    ~Item(){};
};

class Inventar{ //trida inventare
    public:
    std::vector<Item*> inv; //vektor predmetu
        Inventar(){};
        ~Inventar();
        
        //funkce
        void add(Item* a);
        void create();
        void list(int coins);
};

Inventar::~Inventar() //desktruktor
{
	inv.clear();   
}

void Inventar::add(Item * a) // pridava itemy do inventare
{
	inv.push_back(a);
}

void Inventar::create() //vytvari herni inventar pro obchod apod.
{
    add(new Item("mecik", 50, 0,0));
    add(new Item("zabijak", 50, 100, 1));
    add(new Item("zihadlo", 100, 200, 2));
    add(new Item("drakobijec", 150, 300, 3));
    add(new Item("Bum-bum", 200, 400, 4));
    add(new Item("Zlate brneni", 50, 250, 5));
    add(new Item("Milove boty", 25, 250, 6));
    add(new Item("Lektvar (prida 50 HP)", 0, 50, 7));
}

void Inventar::list(int coins) //vypise inventar
{
    sleep(1);
    std::cout << std::string(44, '-') << std::endl;
    std::cout << "Inventar:" << std::setw(15) <<"Penize: " << coins << std::endl;
    std::cout << std::setw(3) 
    << "ID" << std::setw(25) 
    << "Jmeno" << std::setw(10) 
    << "Utok" << std::setw(6) 
    << "Cena" << std::endl;

    for (int i = 0; i < inv.size(); i ++) //vypisovani
    {
	    std::cout << std::setw(3) 
        << inv[i]->id << std::setw(25) 
        << inv[i]->name << std::setw(10) 
        << inv[i]->attack << std::setw(6) 
        << inv[i]->price << std::endl;
    }
}

#endif