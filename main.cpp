//readme
//cas a prachy

#include <iostream>
#include "hra.hpp"
#include <string>
#include <ctype.h>

int main()
{   
    srand(time(0));
    Hra h; //vytvareni hry
    h.inv.create(); //vytvareni inventare pro hru
    h.soubory(); //nacitani souboru (questu)
    h.tutorial();
rozcestnik:
    int i = 1;

    while (1) //smycka hlavniho menu
    {
        std::cout << "\nTak co budeš delat ted hrdino?" << std::endl;
        std::cout << "1: Nejaky ukol" << std::endl
                  << "2: Obchod" << std::endl
                  << "3: Inventar" << std::endl
                  << "4: Staty" << std::endl
                  << "5: Dam si pauzu (konec)" << std::endl;

        std::cin >> h.hlavni_volba;
        //int vol = h.hlavni_volba-48;
        if (h.zkontrolujVstup(h.hlavni_volba, 1, 5) == 1) //osetreni vstupu
        {
            goto rozcestnik;
        }
        int vol = h.hlavni_volba - 48;

        switch (vol) //rozhodovani
        {
        case 1:
            if (i>h.quest.size() -1)
            {
                std::cout << "To uz je vse. Sbohem...";
                return 0;
            }
            
            h.nahraj(h.quest[i]); //nacitani a hrani questu
            h.hraj();
            sleep(5);
            h.protivnik.vytvor(h.protivnik.getName(), h.hrac.level * 50, h.hrac.level * 100, h.hrac.level * 200, h.hrac.level, h.hrac.level * 30); //vytvoreni protivnika pro boj
            h.hrac.boj(h.protivnik); //boj
            i++;
            break;
        case 2:
            h.obchod();
            break;
        case 3:
            h.hrac.inv.list(h.hrac.coins_p);
            break;
        case 4:
            h.hrac.staty();
            break;
        case 5:
            std::cout << "Sbohem..." << std::endl;
            return 0; //konec
            break;
        default:
            break;
        }
    }

    return 0;
}
