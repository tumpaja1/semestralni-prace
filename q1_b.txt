"Vyborne. Ziskal jsi ho. Snad jsi nemel potize", s usklibnutim prohodi Merlin, kdyz mu davas svaty gral. (1) Vubec ne, byla to hracka, (2) No malem jsem tam umrel
2 1 2
"To je dobre. Mam pro tebe totiz dalsi ukol. Tentokrat to asi nebude tak jednoduche. Chci abys mi z laboratore v kralovskem hradu prinesl hul, kterou tam mma mistni carodej. Beres?" (1) Jasne
1 3
"Ale stejne jsi to zvladl. Snad zvladnes i dalsi ukol, ktery pro tebe mam. Chci abys mi z laboratore v kralovskem hradu prinesl hul, kterou tam mma mistni carodej. Beres?" (1) Jasne
1 3
Stojis pred branou do hradu. Pujdes (1) branou a oficielni cestou nebo (2) se tam tajne vkrades?
2 4 10
Prijdes k brane, kde ti cestu zastoupi zbrojnosi. "Co chcete v hradu?", (1) zalzes, (2) reknes pravdu.
2 5 6
"Rad bych neco prostudoval v hradnim archivu", reknes a hezky se usmejes. Nastesti se straze rozestoupi a ty mas volnou cestu do hradu. (1) Pokracovat
1 9
"Jdu se vloupat do kralovske laboratore pro jednu hul starci." Straze se na tebe podivaji a pak se jeden nejiste zepta: "Opravdu?" (1) Jasnacka (2) Ne, delam si srandu
2 7 8
Po tom, ti dali pouta a dostal jsi se do vezeni. Tam po vycerpavajicich trech dnech prichazi Merlin. "To se moc nepovedlo co? Myslel jsem ze to zvladnes. To ja jsem kralovsky caarodej, chtel jsem si te jen vyzkouset. Hul mi nosit nemusis, ale tady mas pilnik a dostan se z vezeni." S temito slovy odesel. Zacnes okamzite s vervou pilovat zamek. Ten po chvili povoli a ty bezis ven. Ohlidnes se a do nekoho narazis. 
0
Straze se rozestoupi, ale kdyz jsi prochazel okolo, dostal jsi poradny kopanec do zadku. (1) Pokracovat
1 9
Bloudis v chodbach az najdes dvere s napisem LABORATOR. Zkusis kliku a ta povoli. Jdes dal a vidis spoustu divnych nestvur v klecich a je tam i par osob, ale nikdo si te zatim nevsiml. Popadnes hul a chces co nejrychleji vypadnout. Radsi uz bezis po chodbe, kdyz zakopnes, hul spadne na zem a vybleskne z ni zeleny blesk a pred tebou se objevi postava, ktera ale nevypada moc pratelsky. Ano uz zase.
0
Rozebehnes se a bezis, co nejrychleji kolem strazi. Jeden ti ale podkopl nohy a ty dostavas nakladacku. Bohuzel. (1) Pokracovat
1 7